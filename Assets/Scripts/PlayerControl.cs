﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerControl : MonoBehaviour
{
    float horMove;
    float verMove;

    public float speed = 10.0f;

    Rigidbody rb;

    private int count;

    public Text countText;
    public Text winText;

    // Start is called before the first frame update
    void Start()
    {
        count = 0;
        rb = rb.GetComponent<Rigidbody>();
        countText.text = "Points: " + count.ToString();
        winText.text = "";
    }

    // Update is called once per frame
    void Update()
    {

    }

    private void FixedUpdate()
    {
        horMove = Input.GetAxis("Horizontal");
        verMove = Input.GetAxis("Vertical");

        Vector3 movement = new Vector3(horMove, 0.0f, verMove);

        rb.AddForce(movement * speed);
    }

    private void LateUpdate()
    {
        
    }

    private void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.CompareTag("Collectible Items"))
        {
            count = count + 1;
            other.gameObject.SetActive(false);
            countText.text = "Points: " + count.ToString();
            if(count>= 12) { winText.text = "You win!"; }
        }
    }
}
